package my.android

import org.apache.commons.configuration.Configuration
import cc.spray.RequestContext
import server.net.RichRequestContext

package object common {
  implicit def configuration2RichConfiguration(enriched: Configuration) = new RichConfiguration(enriched)
  implicit def toRichRequestContext(enriched : RequestContext) = new RichRequestContext(enriched)

}
