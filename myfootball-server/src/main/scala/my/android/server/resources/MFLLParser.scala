package my.android.server.resources

import org.eclipse.jetty.client.ContentExchange
import java.net.URI
import org.htmlcleaner.HtmlCleaner
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
/**
 * Creator: Spiridonov AV
 * Date: 17.11.12: 12:42
 */
object MFLLParser extends App{
  val timeOut = 10000

  val client = new org.eclipse.jetty.client.HttpClient
  client setConnectorType org.eclipse.jetty.client.HttpClient.CONNECTOR_SELECT_CHANNEL
  client setTimeout timeOut
  client setConnectTimeout timeOut
  client setIdleTimeout timeOut
  client setMaxConnectionsPerAddress 20
  client start()

  val siteEncoding = "utf-8"
  val winEncoding = "windows-1251"
  val contentExchange = new ContentExchange()
  contentExchange.setURI(new URI("http://lfl.ru/statistics/index2.php?region_id=10&league_id=21&tournament_id=242"))
  contentExchange.setRequestContentType("Content-Type: text/html; charset=" + winEncoding)
  contentExchange.setRequestHeader("Content-Type", "text/html;charset=" + winEncoding)
  contentExchange.setRequestHeader("Accept", "text/html;charset=" + winEncoding)
  client.send(contentExchange)
  contentExchange.waitForDone()
  val result: String = new String(contentExchange.getResponseContentBytes, winEncoding)

  println(result)
  val cleaner = new HtmlCleaner()
  val cleanedContent = cleaner.clean(result)
  val commandsPositionXPath = "//div[@class='module-title']//span[.='Положение команд']/../../..//table//tr"
  val findChampionshipTable =  cleanedContent.evaluateXPath(commandsPositionXPath)

}