package my.android.server.resources

import javax.ws.rs.core.StreamingOutput
import java.io.OutputStream
import javax.ws.rs.{PathParam, GET, Path}

@Path("/history/{username}")
class History {

  @GET
  def get(@PathParam("username") userName:String) = {
    new StreamingOutput() {
      def write(output: OutputStream) {
        output.write(("Hello" + userName).getBytes)
      }
    }
  }
}