package my.android.server.services

import PlayerSvc._
import my.android.server.net.Authentication
import cc.spray.Directives
import com.google.inject.Inject
import org.apache.commons.configuration.Configuration

@com.google.inject.Singleton
class PlayerSvc @Inject()(val security: Security,
                          val config: Configuration) extends Directives with Authentication{

  val service = {
    pathPrefix(MAIN_PATH) {
      authenticate(restAuthentication) { authCtx =>
        refreshAuthentication(authCtx) {
          path("android") {
            get { ctx =>
              ctx.complete("Authentication completed and now you can get information about user")
            }
          }
        }
      }
    }
  }
}

object PlayerSvc
{
  private [PlayerSvc] val MAIN_PATH = "player"
}