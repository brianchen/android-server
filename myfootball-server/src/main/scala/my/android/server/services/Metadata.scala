package my.android.server.services

/**
 * Creator: Spiridonov AV
 * Date: 17.11.12: 17:32
 */
object Metadata {
  object AccountTypes {
    val VK = "vk"
    val FB = "fb"
  }

}
