// Copyright 2011 Shopolate Inc. All rights reserved.
package my.android.server.services

import com.google.inject.Inject
import java.util.Date
import org.apache.commons.configuration.Configuration
import javax.ws.rs.core.{Cookie, NewCookie}
import my.android.common._
import cc.spray.http.HttpCookie
import my.android.common.{Encryption, MyFootballException}
import org.apache.commons.lang.time.DateFormatUtils

trait UserVerifier {
  def exists(userId: String): Boolean

  def asyncExists(userId: String)(callback: PartialFunction[Boolean, Unit])
}

object Security {

  final val AUTHORIZATION_COOKIE_NAME = "authorization"

  object Roles {
    final val USER = "user"
    final val OWNER = "owner"
    final val VISITOR = "visitor"
    final val ADMIN = "admin"
  }

}

@com.google.inject.Singleton
class Security @Inject()(private val verifier: UserVerifier,
                         private val encryption: Encryption,
                         private val config: Configuration) {

  import Security._

  private val expirationSecs: Int = config.getInt("session.expiration-min", 60) * 60
  private val expirationMillis = expirationSecs * 1000
  private val ADMIN_TOKEN = config.adminToken


  def checkAuthorization(cookie: Cookie): Option[String] = {
    if (cookie != null) {
      try {
        Some(checkAuthorizationHash(cookie.getValue))
      }
      catch {
        case e: Exception => None
      }
    }
    else None
  }

  def checkAuthorizationHash(hash: String): String = {
    val (userId, exp) = extractHashComponents(hash)

    if (!verifier.exists(userId)) throw UserDoesNotExistException(userId)

    val expiration = exp.toLong

    val currentDate: Long = new Date().getTime
    val currentDateStr = DateFormatUtils.format(currentDate,"dd MMM yyyy HH:mm:ss")
    val currentDateExp = DateFormatUtils.format(expiration,"dd MMM yyyy HH:mm:ss")

    if (expiration < currentDate) throw AuthorizationExpiredException

    userId
  }

  def asyncCheckAuthorizationHash(hash: String)(callback: PartialFunction[Either[MyFootballException, String], Unit]) {
    try {
      val (userId, exp) = extractHashComponents(hash)

      verifier.asyncExists(userId) {
        case false =>
          callback(Left(UserDoesNotExistException(userId)))
        case true =>
          if (exp.toLong < new Date().getTime)
            callback(Left(AuthorizationExpiredException))
          else
            callback(Right(userId))
      }
    }
    catch {
      case se: MyFootballException => callback(Left(se))
      case other => callback(Left(MalformedHashException))
    }
  }

  def createAuthorizationHash(userId: String): String = {
    if (userId.contains(":")) throw InvalidUserIdException

    val toEncode = String.format("%s:%s", userId, (new Date().getTime + expirationMillis).toString)

    encryption.symmetricEncodeStringInBase64Url(toEncode)
  }


  def createAuthorizationCookie(userId: String): NewCookie = {
    val hash = createAuthorizationHash(userId)
    new NewCookie(AUTHORIZATION_COOKIE_NAME, hash, "/", null, null, expirationSecs, false)
  }

  def createRemoveAuthorizationCookie = new NewCookie(AUTHORIZATION_COOKIE_NAME, "", "/", null, null, 1, false)

  def refreshAuthenticationCookie(userId: Option[String]): HttpCookie = userId match {
    case Some(id) => HttpCookie(AUTHORIZATION_COOKIE_NAME, createAuthorizationHash(id), path = Some("/"), maxAge = Some(expirationSecs))
    case None => HttpCookie(AUTHORIZATION_COOKIE_NAME, "", path = Some("/"), maxAge = Some(1))
  }

  private def extractHashComponents(hash: String): Pair[String, String] = {
    val decrypted = encryption.symmetricDecodeStringInBase64Url(hash)

    if (decrypted.indexOf(':') < 0) throw MalformedHashException

    val hashComponents = decrypted.split(':')

    Pair(hashComponents(0), hashComponents(1))
  }
}

case object InvalidUserIdException extends MyFootballException("A user id must not contain the ':' character to be valid.")

case object MalformedHashException extends MyFootballException("")

case object AuthorizationExpiredException extends MyFootballException("")

case class UserDoesNotExistException(userId: String) extends MyFootballException("User %s does not exist.", userId)