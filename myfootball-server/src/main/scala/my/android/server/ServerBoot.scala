package my.android.server

import com.google.inject.Guice
import com.google.inject.servlet.{ServletModule, GuiceServletContextListener}
import org.apache.commons.configuration._
import akka.actor.Actor
import cc.spray.RootService
import my.android.model.{MongoActor, InMemoryModelActor, ModelUserVerifier}
import my.android.common.{Container, GuiceContainer, Encryption}
import services.common.RestHttpService
import services.{GeoSvc, AuthorizationSvc, UserVerifier, PlayerSvc}
import akka.config.Supervision.{OneForOneStrategy, Supervise, Permanent}
import akka.actor.SupervisorFactory
import akka.config.Supervision.SupervisorConfig


object ServerBoot{

   lazy val injector = Guice.createInjector(new ServletModule {
     override def configureServlets() {
       val config = new CompositeConfiguration
       config addConfiguration(new SystemConfiguration)
       config addConfiguration(new XMLConfiguration(ConfigurationUtils locate "common.xml"))

       bind(classOf[Container]) to classOf[GuiceContainer]
       bind(classOf[UserVerifier]) to classOf[ModelUserVerifier]
       bind(classOf[Configuration]) toInstance config
       bind(classOf[Encryption])


       //Services
       bind(classOf[PlayerSvc])
       bind(classOf[AuthorizationSvc])
       bind(classOf[GeoSvc])

       //bind(classOf[History]) // resource
       //Сработает если в модуле есть хотя бы один ресурс (аннотация @Path)
       //например: bind(classOf[History])
       //serve("*").`with`(classOf[com.sun.jersey.guice.spi.container.servlet.GuiceContainer])
     }
   })

    Container.instance = injector.getInstance(classOf[Container])

   val player = injector.getInstance(classOf[PlayerSvc]).service
   val authorization = injector.getInstance(classOf[AuthorizationSvc]).service
   val geo = injector.getInstance(classOf[GeoSvc]).service

   val playerActor = Actor.actorOf(RestHttpService(player)).start()
   val authActor = Actor.actorOf(RestHttpService(authorization)).start()
   val geoActor = Actor.actorOf(RestHttpService(geo)).start()

   SupervisorFactory(
     SupervisorConfig(
       OneForOneStrategy(List(classOf[Exception])),
       Supervise(Actor.actorOf(new RootService(authActor, playerActor, geoActor)).start(), Permanent) ::
       Supervise(Actor.actorOf(new MongoActor()).start(), Permanent) ::
       Supervise(Actor.actorOf(new InMemoryModelActor()).start(), Permanent) ::  Nil
     )
   ).newInstance

}

class ServerBoot extends GuiceServletContextListener {
  def getInjector = ServerBoot.injector
}
