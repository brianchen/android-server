package my.android.server.net

import cc.spray._
import http.HttpHeaders._
import cc.spray.Directives
import org.apache.commons.configuration.Configuration
import my.android.server.services.Security
import akka.dispatch.Future
import my.android.common._

trait Authentication extends Directives {
  val security: Security
  val config: Configuration

  def restAuthentication: GeneralAuthenticator[AuthenticationContext] = reqCtx => {
    val res = new AuthenticationContext

    res.userId = reqCtx.cookie(Security.AUTHORIZATION_COOKIE_NAME).map(security.checkAuthorizationHash)

    Future(Right(res))
  }

  def refreshAuthentication(authCtx: AuthenticationContext) = transformResponse {
    resp =>
      resp.copy(headers = `Set-Cookie`(security.refreshAuthenticationCookie(authCtx.userId)) :: resp.headers)
  }
}

class AuthenticationContext {
  var userId: Option[String] = None
  var isAdmin: Boolean = false

  def isLoggedIn = userId.isDefined

  def is(id: String) = isLoggedIn && userId.get == id
}
