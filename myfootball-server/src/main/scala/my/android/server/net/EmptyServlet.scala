package my.android.server.net

import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}

class EmptyServlet extends HttpServlet {
  override protected def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
    throw new IllegalStateException("unable to service request");
  }
}