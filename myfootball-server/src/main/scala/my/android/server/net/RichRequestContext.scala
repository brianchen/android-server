package my.android.server.net

import cc.spray._
import http._
import http.HttpHeaders.Cookie
import StatusCodes._
import typeconversion._

class RichRequestContext(enriched: RequestContext) {
  def complete[A: Marshaller](obj: A, firstHeader: HttpHeader, nextHeaders: HttpHeader*) {
    marshaller.apply(enriched.request.acceptableContentType) match {
      case MarshalWith(converter) => enriched.complete(OK, firstHeader :: nextHeaders.toList, obj)
      case CantMarshal(onlyTo) => enriched.reject(UnacceptedResponseContentTypeRejection(onlyTo))
    }
  }

  def cookie(name: String) = {
    val headers: List[HttpHeader] = enriched
      .request
      .headers
    val filter: List[HttpHeader] = headers.filter(hdr => hdr.isInstanceOf[Cookie])
    val cook = filter.flatMap {
      c => {
        val cookies: Seq[HttpCookie] = c.asInstanceOf[Cookie].cookies
        val cookfind: Option[HttpCookie] = cookies.find(_.name == name)
        val cont: Option[String] = cookfind.map(_.content)
        cont
      }
    }
    cook.headOption
  }
}