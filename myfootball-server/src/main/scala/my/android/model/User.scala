package my.android.model

import collection.immutable
import collection.immutable.HashMap

case class User(login:String,
            fullname: String = "",
             password: String = "",
              avatarUrl: String = ""
            ) {
     var coordinates = null
     var activeZonesIds = new HashMap[String,Zone]
}
