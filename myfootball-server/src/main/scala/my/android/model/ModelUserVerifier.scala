package my.android.model

import my.android.server.services.UserVerifier
import my.android.common.ModelConsumer

class ModelUserVerifier extends UserVerifier with ModelConsumer {
  def exists(userId: String) = {
    getModel[Option[User]](GetUserReply(userId)) match {
      case Some(_) => true
      case None => false
    }
  }

  def asyncExists(userId: String)(callback: PartialFunction[Boolean, Unit]) {
    actor ! GetUser(userId, {
      case op => callback(op.isDefined)
    })
  }
}