package my.android.net

import org.mozilla.universalchardet.UniversalDetector
import java.lang.String

/**
 * Creator: Spiridonov AV
 * Date: 17.11.12: 15:26
 */
class EncodingTest {

  val detector = new UniversalDetector(null)
  val data = ""
  detector.handleData(data.getBytes, 0 ,data.size )
  detector.dataEnd()
  val detectedCharset = detector.getDetectedCharset
  val convertedData = new String(data.getBytes, "utf-8")
}
