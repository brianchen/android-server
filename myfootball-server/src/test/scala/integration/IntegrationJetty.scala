import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext

object IntegrationJetty {

  def main(args : String*)
  {
    val jettyHome = System.getProperty("jetty.home", "src/test/scala");

    val server = new Server(8080);

    val webapp = new WebAppContext();
    webapp.setContextPath("/");
    webapp.setWar(jettyHome + "/webapps/test.war");
    server.setHandler(webapp);

    server.start();
    server.join();
  }
}
