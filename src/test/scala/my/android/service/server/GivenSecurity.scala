package my.android.service.server

import my.android.IntegrationTestFixture
import java.util.Date
import org.joda.time.DateTime
import org.apache.commons.lang.time.DateFormatUtils

class GivenSecurity extends IntegrationTestFixture {

  "An InMemoryModel actor" must {

    "must recieve message from Authentication/Registration service" in {

      val expirationMillis = new DateTime().plusYears(1).getMillis
      val currentDateStr = DateFormatUtils.format(expirationMillis,"dd MMM yyyy HH:mm:ss")
      val userId = "deil87"
      val toEncode = String.format("%s:%s", userId, (expirationMillis).toString)

       val encodedUserInfo = encryption.symmetricEncodeStringInBase64Url(toEncode)

      encodedUserInfo must be ("UmdIEVwNfcW0EfALWfrSRsJQqpUz3NJRb5-OhmgD0HY")

    }
  }
}
