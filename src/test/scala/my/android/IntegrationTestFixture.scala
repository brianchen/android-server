package my.android

import akka.actor.Actor
import cc.spray.utils.Logging
import com.google.inject.Guice
import common.{GuiceContainer, Container, Encryption}
import my.android.common.Container._
import org.apache.commons.configuration._
import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import com.google.inject.servlet.ServletModule
import my.android.model.{InMemoryModelActor, ModelUserVerifier}
import my.android.server.services.UserVerifier

trait IntegrationTestFixture extends MustMatchers with WordSpec with Logging{
  try {
    val injector = Guice.createInjector(new ServletModule() {
      override def configureServlets() {
        val config = new CompositeConfiguration
        config addConfiguration(new SystemConfiguration)
        config addConfiguration(new XMLConfiguration(ConfigurationUtils locate "common.xml"))

        bind(classOf[Container]) to classOf[GuiceContainer]
        bind(classOf[UserVerifier]) to classOf[ModelUserVerifier]
        bind(classOf[Configuration]) toInstance config
        bind(classOf[Encryption])
      }
    })

    Container.instance = injector.getInstance(classOf[Container])
  }
  catch {
    case t: Throwable =>
      log error("Test container initialization error", t)
      throw t
  }

  val testInMemoryActor = Actor.actorOf(new InMemoryModelActor()).start()

  val encryption = instance[Encryption]
}
