package my.android.common

import akka.actor.{ActorRef, Actor}

object Container {
  var instance: Container = null
}

/**
 * An IoC container abstraction.
 */
trait Container {
  def get[T](clasz: Class[T]): T

  def apply[T](implicit manifest: Manifest[T]) = get[T](manifest.erasure.asInstanceOf[Class[T]])
}