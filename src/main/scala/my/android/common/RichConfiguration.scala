// Copyright 2011 Shopolate Inc. All rights reserved.
package my.android.common

import org.apache.commons.configuration.Configuration

class RichConfiguration(private val enriched: Configuration) {
  lazy val environment = enriched.getString("environment")
  lazy val facebookApiSecret = enriched.getString("facebook.apiSecret")
  lazy val adminToken = enriched.getString("rest.adminToken")
  lazy val eventStoreFilePath = enriched.getString("eventStore.filePath")
  lazy val resetAtStartup = enriched.getBoolean("eventStore.resetAtStartup", false)
  // Whether we must find an history when starting from scratch (first start, or crash recovery)
  lazy val mustFindHistory = enriched.getBoolean("eventStore.mustFindHistory", false)
}