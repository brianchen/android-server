package my.android.server.net

import org.eclipse.jetty.servlet.{FilterHolder, ServletContextHandler}


class ServletContextHandlerAdapter extends ServletContextHandler{

  /* ------------------------------------------------------------ */
  /**convenience method to add a filter
   */
  def addFilter(filterClass: String, pathSpec: String, dispatches: Int): FilterHolder = {
      getServletHandler.addFilterWithMapping(filterClass, pathSpec, dispatches)
  }
}
