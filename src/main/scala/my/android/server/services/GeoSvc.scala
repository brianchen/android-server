package my.android.server.services

import com.google.inject.Inject
import org.apache.commons.configuration.Configuration
import cc.spray.Directives
import my.android.server.net.Authentication
import GeoSvc._
import akka.actor.Actor
import my.android.model.GetGeoDistance
import com.codahale.jerkson.Json._

case class GeoPoint(lat: Double, lng: Double)

@com.google.inject.Singleton
class GeoSvc @Inject()(val security: Security,
                       val config: Configuration) extends Directives with Authentication {

  lazy val actor = Actor.registry.actorsFor("model").head

  val service = {
    pathPrefix(MAIN_PATH) {
      path("distance") {
        get {
          ctx => {
            actor ! GetGeoDistance({
              case Some(distance) => ctx.complete("Distance:" + distance)
            })
          }
        }
      } ~
      path("coords") {
        get {
          ctx => {
            ctx.complete(generate(List(GeoPoint(55.45, 37.45),GeoPoint(35.45, 27.45))))
          }
        }
      }
    }
  }
}

object GeoSvc {
  private[GeoSvc] val MAIN_PATH = "geo"
}