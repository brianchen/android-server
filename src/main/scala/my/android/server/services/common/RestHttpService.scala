package my.android.server.services.common

import cc.spray.{HttpServiceActor, RejectionHandler, HttpServiceLogic, Route}
import cc.spray.utils.Logging


trait RestHttpServiceLogic extends HttpServiceLogic
{
  this: Logging =>

  def rejectionHandler = RejectionHandler.Default
}

case class RestHttpService(route: Route) extends HttpServiceActor with RestHttpServiceLogic


